import requests


class IgnitionFuelApi:

    BASE_URL = 'https://fuel.ignitionrobotics.org/1.0'

    def __init__(self, token: str):
        self.token = token

    def create_model(self, files: dict, model_name: str, is_model_privat: bool) -> None:
        headers = {
            'Private-token': self.token,
        }

        payload = {
            'license': 1,
            'name': model_name,
            'private': is_model_privat,
        }

        files_prepare = []
        for file in files:
            files_prepare.append(
                ('file', (
                    file, files[file],
                    'application/octet-stream'))
            )

        response = requests.post(f'{self.BASE_URL}/models', headers=headers, files=files_prepare, data=payload)

        if response.status_code != 200:
            print(f'Response {str(response.status_code)}: {response.text}')
