import subprocess


class GitHelper:

    @staticmethod
    def get_git_hash():
        try:
            commit_hash = subprocess.check_output(
                ['git', 'rev-parse', '--short', 'HEAD']).decode().strip()
        except subprocess.CalledProcessError:
            commit_hash = '0000000'

        return commit_hash

    @staticmethod
    def get_git_tag():
        try:
            tag = subprocess.check_output(
                ['git', 'tag']).decode().strip()
        except subprocess.CalledProcessError:
            tag = '0000000'

        return tag

    @staticmethod
    def get_repo_name():
        try:
            repo_name = subprocess.check_output(
                ['git', 'rev-parse', '--show-toplevel']).decode().strip()
        except subprocess.CalledProcessError:
            repo_name = '0000000'

        return repo_name


if __name__ == '__main__':
    print(GitHelper.get_git_hash())
    print(GitHelper.get_git_tag())
    print(GitHelper.get_repo_name())
