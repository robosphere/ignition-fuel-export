import io
import os
from PIL import Image
from .geometric_objects.abstract_geometry_object import AbstractGeometryObject
from .git_helper import GitHelper

TEXTURES_JPG = (
    'Plastic010_1K_Color.jpg',
    'Plastic010_1K_Displacement.jpg',
    'Plastic010_1K_NormalGL.jpg',
    'Plastic010_1K_Roughness.jpg',
)

TEXTURES_PNG = (
    'Plastic010_1K_Color.png',
    'Plastic010_1K_Displacement.png',
    'Plastic010_1K_NormalGL.png',
    'Plastic010_1K_Roughness.png',
)


class DataConverter:

    def __init__(self, git_helper: GitHelper):
        self.git_helper = git_helper

    def get_prepare_data(self, geometry_obj: AbstractGeometryObject) -> dict:
        stl = geometry_obj.export_stl()
        dae = geometry_obj.export_collada()
        hash_assembly_unit = geometry_obj.metadata['hash_assembly_unit'] \
            if 'hash_assembly_unit' in geometry_obj.metadata \
            else ''
        hash_detail_identifier = geometry_obj.metadata['hash_detail_identifier'] \
            if 'hash_detail_identifier' in geometry_obj.metadata \
            else ''
        model_name = geometry_obj.metadata['model_name'] \
            if 'model_name' in geometry_obj.metadata \
            else ''
        config = self.__get_config_file(
            model_name,
            self.git_helper.get_git_hash(),
            self.git_helper.get_git_tag(),
            self.git_helper.get_repo_name(),
            hash_assembly_unit,
            hash_detail_identifier
        )
        sdf = self.__get_sdf_file(geometry_obj.get_moment_inertia(), geometry_obj.get_mass(), model_name)

        result = {
            f'meshes/{model_name}.stl': stl,
            f'meshes/{model_name}.dae': dae,
            'model.config': config,
            'model.sdf': sdf,
        }

        self.__add_textures(result)

        return result

    def __add_textures(self, result):
        for texture in TEXTURES_PNG:
            picture = self.__read_picture(f'{os.getcwd()}/textures/{texture}', 'PNG')
            result[f'/materials/textures/{texture}'] = picture

    def __get_config_file(self,
                          model_name: str,
                          git_commit_hash: str,
                          git_tag: str,
                          git_repo_name: str,
                          hash_assembly_unit: str,
                          hash_detail_identifier: str
                          ) -> str:
        return f"""
        <?xml version='1.0'?>
        <model>
            <name>{model_name}</name>
            <version>1.0.0</version>
            <sdf version='1.6'>model.sdf</sdf>
            <description>
                git_commit_hash: {git_commit_hash}
                git_tag: {git_tag}
                git_repo_name: {git_repo_name}
                hash_assembly_unit: {hash_assembly_unit}
                hash_detail_identifier: {hash_detail_identifier}
            </description>
        </model>
        """

    def __get_sdf_file(self,
                       moment_inertia: dict,
                       mass: str,
                       model_name: str,
                       ) -> str:
        return f"""
        <?xml version="1.0"?>
        <sdf version="1.6">
            <model name="{model_name}">
            <pose>0 0 0 0 0 0</pose>
                <static> true </static>
                <link name="link">
                    <visual name="visual">
                        <geometry>
                            <mesh>
                                <scale>1 1 1</scale>
                                <uri>meshes/{model_name}.dae</uri>
                            </mesh>
                        </geometry>
                        <material>
                            <diffuse>1.0 1.0 1.0</diffuse>
                            <ambient>1.0 1.0 1.0</ambient>
                            <pbr>
                                <metal>
                                    <albedo_map>https://fuel.ignitionrobotics.org/1.0/miniaevnikita/models/{model_name}/1/files/materials/textures/Plastic010_1K_Color.png</albedo_map>
                                    <metalness_map>https://fuel.ignitionrobotics.org/1.0/miniaevnikita/models/{model_name}/1/files/materials/textures/Plastic010_1K_Displacement.png</metalness_map>
                                    <roughness_map>https://fuel.ignitionrobotics.org/1.0/miniaevnikita/models/{model_name}/1/files/materials/textures/Plastic010_1K_Roughness.png</roughness_map>
                                    <normal_map>https://fuel.ignitionrobotics.org/1.0/miniaevnikita/models/{model_name}/1/files/materials/textures/Plastic010_1K_NormalGL.png</normal_map>
                                </metal>
                            </pbr>
                        </material>
                    </visual>
                    <collision name="collision">
                        <geometry>
                            <mesh>
                                <uri>meshes/{model_name}.stl</uri>
                            </mesh>
                        </geometry>
                    </collision>
                    <inertial>
                        <pose>0 0 0 0 0 0</pose>
                        <mass>{mass}</mass>
                        <inertia>
                            <ixx>{moment_inertia['xx']}</ixx>
                            <iyy>{moment_inertia['yy']}</iyy>
                            <izz>{moment_inertia['zz']}</izz>
                            <ixy>{moment_inertia['xy']}</ixy>
                            <ixz>{moment_inertia['xz']}</ixz>
                            <iyz>{moment_inertia['yz']}</iyz>
                        </inertia>
                    </inertial>                    
                </link>
            </model>
        </sdf>
        """

    def __read_picture(self, path: str, file_type: str) -> bytes:
        im = Image.open(path)
        im_resize = im.resize((2500, 2500))
        buf = io.BytesIO()
        im_resize.save(buf, format=file_type)
        return buf.getvalue()
